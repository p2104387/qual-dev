/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entree;

/**
 *
 * @author p2104387
 */
public class Personne {
    private String prenom[];
    private String nom;
    private Genre genre;
    private Personne conjoint;
    private Société société;
    private String fonction;
    
    public personne (String prenom[],String nom,Genre genre,Personne conjoint,Société société,String fonction){
        
    }
    public Société getSociété() {
        return société;
    }

    public void setSociété(Société société) {
        this.société = société;
    }
    public String[] getPrenom() {
        return prenom;
    }

    public void setPrenom(String[] prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public entree.Personne getConjoint() {
        return conjoint;
    }

    public void setConjoint(entree.Personne conjoint) {
        this.conjoint = conjoint;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "Personne{" +"prenom=" + prenom + ", nom='" + nom + '\'' + ", genre=" + genre + ", conjoint=" + conjoint + ", société=" + société + ", fonction='" + fonction + '\'' + '}';
    }

    public String toString(Presentation pres, Sens sens) {
        if (pres= ABREGE){
            if (sens=NOM_PRENOM){
                return "Personne{" +"nom=" + nom +", prenom=" + prenom + '}';
            }
            else{
                return "Personne{" +"prenom=" + prenom + ", nom='" + nom +'}';
            }
        }
        if (pres = SIMPLE){
            if (sens=NOM_PRENOM){
                return "Personne{" +"nom=" + nom +", prenom=" + prenom + '\'' + ", genre=" + genre + ", société=" + société + '}';
            }
            else{
                return "Personne{" +"prenom=" + prenom + ", nom='" + nom + '\'' + ", genre=" + genre + ", société=" + société + '}';
            }
        }
        if (pres= COMPLET){
            if (sens=NOM_PRENOM){
                return "Personne{" +"nom=" + nom +", prenom=" + prenom +  '\'' + ", genre=" + genre + ", conjoint=" + conjoint + ", société=" + société + ", fonction='" + fonction + '\'' + '}';
            }
            else{
                return "Personne{" +"prenom=" + prenom + ", nom='" + nom + '\'' + ", genre=" + genre + ", conjoint=" + conjoint + ", société=" + société + ", fonction='" + fonction + '\'' + '}';
            }
        }

    }

}
